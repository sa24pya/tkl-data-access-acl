<?php
/**
 * File: tests/ACLTest.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataAccessACL
 * @subpackage ACL
 * @version 1.0.0
 *
 */

namespace DataAccessACL;

use Dotenv\Dotenv;
use DataAccessDAL\DAL as DAL;

/**
 * Class ACLTest
 *
 * @package DataAccessACL
 * @subpackage ACL
 * @version 1.0.0
 */
class ACLTest extends \PHPUnit_Framework_TestCase
{


    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    /**
     * Server URL
     * @var string
     */
    const SERVER_URL = "http://m2.manager.shobr.com" ;

    public function setUp()
    {
        DAL::getInstance()->setCollections('GS1_test', 'SA24_test');
    }
    public function tearDown()
    {
        // Drop collection after testing:
        $mongoServer = 'localhost';
        $mongoPort = 27017;
        $manager = new \MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");
        $command = new \MongoDB\Driver\Command([
            'drop' => 'person',
        ]);
        $manager->executeCommand('SA24', $command);
    }

    private function populatePersonDB($email = 'eko@shopall24.com', $password = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9')
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>$email,
            'password'=>$password,
            'ACLRole'=>'Supplier_Editor',
            'glns'=> ['5790000010974']
        ];
        $dal = DAL::getInstance();
        $dal->personSave((object)$person);
        $person =[
            'client'=>'shobrManager',
            'email'=> rand(0, 5000) . '@' . rand(0, 5000) . '.org',
            'password'=> '123',
            'ACLRole'=> 'Supplier_Editor',
            'glns'=> '11110000010974'
        ];
        $dal = DAL::getInstance();
        return $dal->personSave((object)$person);
    }


    public function testPersonAuthenticate()
    {
        $this->populatePersonDB();
        $credentials = ['email'=>'eko@shopall24.com', 'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'];

        $acl = new \DataAccessACL\ACL();
        $result = $acl->personAuthenticate($credentials);
        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists('person', $result));
        $this->assertTrue(array_key_exists('token', $result));
        $this->assertTrue(!empty($result['token']));
        $this->assertTrue(!empty($result['person']));
        $credentials = ['email'=>'eko@shopall24.com', 'password'=>'1234'];
        $acl = new \DataAccessACL\ACL();
        $result = $acl->personAuthenticate($credentials);
        $this->assertFalse(is_array($result));
    }


    public function testPersonTokenCreate()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com',
            '_id'=>"123"
        ];
        $acl = new \DataAccessACL\ACL();
        $result = $acl->personTokenCreate((object) $person);
        $this->assertTrue(array_key_exists('code', (array)$result));
        $this->assertTrue(array_key_exists('expire', (array)$result));
    }



    /**
     * @todo fix (in code)
     */
    public function testCatalogueitemSave()
    {
        $email = 'eko@shopall24.com';
        $password = '123';
        // Create person in ACL
        $this->populatePersonDB($email, $password);
        $person =[
            'email'=> $email,
            'password' => $password

        ];
        $acl = new \DataAccessACL\ACL();
        $result = $acl->personAuthenticate($credentials);
        $person = $result["person"];
        $acl->setPerson($person);

        // save catalogue Item
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790000010974',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];
        $acl->catalogueItemSave(['document' => $catalogueOriginal]);
        // uhm...
        $result = $acl->catalogueItemRead([ 'filters'=> ['tradeItem.gtin' => '424242']]);
        // unset($result['SA24'][0]->_id);

        $this->assertEquals(
            json_decode(json_encode($result['SA24'][0]), true),
            (array) $catalogueOriginal
        );
    }


    public function testCatalogueitemPush()
    {
        // Create person in ACL
        $credentials =[
            'email'=> 'mmj@shopall24.com',
            'password' => '123'
        ];
        $acl = new \DataAccessACL\ACL();
        $result = $acl->personAuthenticate($credentials);
        $person = $result["person"];
        $acl->setPerson($person);

        // save catalogue Item
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '5790001106485',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '0000',
            ]
        ];
        $acl->catalogueItemPush(['document' => $catalogueOriginal]);

        $result = $acl->catalogueItemRead([ 'filters'=> ['tradeItem.gtin' => '0000']] );
        unset( $result['SA24'][1]->_id);

        $this->assertTrue(
            isset($result['SA24'][1]->push->pending ) && $result['SA24'][1]->push->pending == true
        );
    }

    public function testStockRead()
    {
        $dotenv = new Dotenv(dirname(__DIR__));
        $dotenv->load();

        // Create person in ACL
        $email = 'eko@shopall24.com';
        $password = '123';
        // Create person in ACL
        $this->populatePersonDB($email, $password);
        $person =[
            'email'=> $email,
            'password' => $password
        ];
        $acl = new \DataAccessACL\ACL();
        $result = $acl->personAuthenticate($person);

        $result = $acl->stockRead([ 'filters'=> ['glns' => '5790000010974']]);
        $this->assertTrue(!empty($result));
    }
}
