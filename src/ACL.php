<?php
/**
 * File: API.php
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat, mrx
 *
 * @package DataAccessACL
 * @subpackage ACL
 * @version 1.0.3
 */

namespace DataAccessACL;

// User Data Access Layer
use DataAccessDAL\DAL;
use DataAccessStockReader\StockReader;

/**
 * Class ACL
 * The Access Control List, or ACL, is responsible for
 * granting or denying access or the data-storage,
 * depending on user and access’ rules.
 *
 * @package DataAccessACL
 * @subpackage ACL
 * @version 1.0.3
 */
class ACL
{
    /**
     * Database Access Layer
     * @var object
     */
    public $dal ;

    /**
     * Person that is making the request.
     * @var object
     */
    public $person;


    /**
     * ACL rules
     * @var json-decode
     */
    private $ACLRules ;

    /**
     * Magic method
     *
     * @param string $serviceName
     * @param array $arguments
     * @throws Exception
     */
    public function __call($serviceName, $args)
    {
        $result = null;
        switch ($serviceName) {
            case 'catalogueItemRead':
                // GLN Limits
                $person = $this->person;
                switch ($this->person->ACLRole) {
                    case 'SA24_Owner':
                        $args[0]['filters'] = array();
                        break;
                    case 'Supplier_Editor':
                        // GLN
                        $filtersAllGln = array();
                        if (!empty($person->glns)) {
                            $gln = $person->glns;
                            $filterGln = ['tradeItem.informationProviderOfTradeItem.gln'=> $gln[0] ];
                            $reqFilters = $args[0]['filters'];
                            $filtersAllGln = array_merge($reqFilters, $filterGln);
                        }
                        // CVR number
                        $filtersAllCvrNumber = array();
                        if (!empty($person->cvrNumbers)) {
                            $cvrNumber = $person->cvrNumbers;
                            $filterCvrNumber = ['tradeItem.informationProviderOfTradeItem.vat.cvrNumber.@value'=> $cvrNumber ];
                            $reqFilters = $args[0]['filters'];
                            $filtersAllCvrNumber = array_merge((array)json_decode($reqFilters), $filterCvrNumber);
                        }
                        $filtersAll = array_merge($filtersAllGln, $filtersAllCvrNumber);

                        $args[0]['filters'] = $filtersAll;
                        break;
                    default:
                        $message = "Person without ACLRole: " . var_export($this->person, true);
                        throw new \Exception($message, 500);
                }
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->catalogueItemRead($args[0]);
                break;
            case 'catalogueItemPush':
                // GLN Limits
                $person = $this->person;
                switch ( $this->person->ACLRole ) {
                    case 'SA24_Owner':
                        break;
                    case 'Supplier_Editor' :
                        // GLN
                        if(!empty($person->glns)) {
                            $gln = $person->glns;
                            $document = $args[0]['document'];
                            // convert to array recursively
                            $document = json_decode(json_encode($document), true);
                            $docGln = $document['tradeItem']['informationProviderOfTradeItem']['gln'];
                            if(!empty($docGln) && $gln[0] != $docGln) {
                                $message = "The person making the request is not the owner of the product."
                                    . " product GLN: '$docGln', person GLN: '$gln'" ;
                                throw new \Exception($message);
                            }
                        }
                        // cvrNumber
                        if(!empty($person->cvrNumbers)) {
                            $cvrNumber = $person->cvrNumbers;
                            $document = $args[0]['document'];
                            // convert to array recursively
                            $document = json_decode(json_encode($document), true);
                            $docCvrNumber = $document ['tradeItem']['informationProviderOfTradeItem']['vat']['cvrNumber']['@value'];
                            if(!empty($docCvrNumber) && $cvrNumber != $docCvrNumber) {
                                $message = "The person making the request is not the owner of the product."
                                    . " product CVR n.: '$docCvrNumber', person CVR n.: '$cvrNumber'" ;
                                throw new \Exception($message);
                            }
                        }
                        break;
                    default:
                        $message = "Person without ACLRole: " . var_export($this->person, true);
                        throw new \Exception($message, 500);
                }
                // DAL
                $document = (array)$args[0]['document'];
                $document["push"] = ["pending"=>"true"];
                $dal = DAL::getInstance();
                $result = $dal->catalogueItemSave((object)$document);
                break;
            case 'personRead' /*'personAuthenticate'*/ :
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName($args[0]);
                break;
            case 'tokenCreate' /*'personAuthenticate'*/ :
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName();
                break;
            case 'tokenRead' /*'personAuthenticate'*/ :
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName($args[0]);
                break;
            case 'tokenDelete' /*'personAuthenticate'*/ :
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName($args[0]);
                break;

            case 'personTokenDelete':
            case 'catalogueItemDelete':
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName($args[0]);
                break;

            /** @todo case 'personSave': **/

            case 'catalogueItemSave':
                // GLN Limits
                $person = $this->person;
                switch ($this->person->ACLRole) {
                    case 'SA24_Owner':
                        $dal = DAL::getInstance();
                        $result = $dal->$serviceName($args[0]['document']);
                        break;
                    case 'Supplier_Editor':
                        // GLN
                        if (!empty($person->glns)) {
                            $gln = $person->glns;
                            $document = $args[0]['document'];
                            // convert to array recursively
                            $document = json_decode(json_encode($document), true);
                            $docGln = $document['tradeItem']['informationProviderOfTradeItem']['gln'];
                            if (!empty($docGln) && !in_array($docGln, $gln)) {
                                $message = "The person making the request is not the owner of the product."
                                    . " product GLN: '".json_encode($docGln)."', person GLN: '".json_encode($gln)."'" ;
                                throw new \Exception($message);
                            }
                        }
                        // cvrNumber
                        if (!empty($person->cvrNumbers)) {
                            $cvrNumber = $person->cvrNumbers;
                            $document = $args[0]['document'];
                            // convert to array recursively
                            $document = json_decode(json_encode($document), true);
                            $docCvrNumber = $document ['tradeItem']['informationProviderOfTradeItem']
                                ['vat']['cvrNumber']['@value'];
                            if (!empty($docCvrNumber) && $cvrNumber != $docCvrNumber) {
                                $message = "The person making the request is not the owner of the product."
                                    . " product CVR n.: '$docCvrNumber', person CVR n.: '$cvrNumber'" ;
                                throw new \Exception($message);
                            }
                        }
                        break;
                    default:
                        $message = "Person without ACLRole: " . var_export($this->person, true);
                        throw new \Exception($message, 500);
                }
                // DAL
                $dal = DAL::getInstance();
                $result = $dal->$serviceName($args[0]['document']);
                break;
            case "stockRead":
                $person = $this->person;
                switch ($this->person->ACLRole) {
                    case 'SA24_Owner':
                        if (!empty($person->glns)) {
                            $filters = $args[0]['filters'];
                            $filters = json_decode($filters);
                            if ($filters->glns) {
                                $result = StockReader::stockRead($filters->glns);
                            } else {
                                $result = StockReader::stockRead();
                            }
                        }
                        break;
                    case 'Supplier_Editor':
                        $filters = new \stdClass();
                        if (is_string(($args[0]['filters']))) {
                            $filtersA = json_decode($args[0]['filters'], true) ;
                        }
                        if (is_array($args[0]['filters'])) {
                            $filtersA = $args[0]['filters'] ;
                        }

                        foreach ($filtersA as $key => $value) {
                            $filters->$key = $value;
                        }
                        if (!empty($person->glns)) {
                            if ($filters->glns) {
                                // if ($filters->glns != $person->glns) {
                                if (!in_array($filters->glns, $person->glns)) {
                                    $message = "Your gln filter is not valid";
                                    throw new \Exception($message, 401);
                                } else {
                                    $result = StockReader::stockRead($filters->glns);
                                }
                            } else {
                                $result = null;
                            }
                        }
                        break;
                    default:
                        $message = "Person without ACLRole: " . var_export($this->person, true);
                        throw new \Exception($message, 500);
                }

                break;
            case "orderRead":
                $person = $this->person;
                if (!empty($person->glns)) {
                    $result = OrderList::orderRead($person->glns);
                }
                break;
            case "orderWrite":
                $person = $this->person;
                $document = $args[0]['document'];
                if (!empty($person->glns) && $document) {
                    $result = OrderList::orderWrite($person->glns, $document);
                }
                break;
            default:
                $message = "Service $serviceName not implemented";
                throw new \Exception($message);
        }
        return $result;
    }


    /**
     * Person Authenticate
     * @param array $args
     */
    public function personAuthenticate(array $credentials)
    {
        $sanitize = ['email' => null , 'password' => null];
        $filters = array_replace($sanitize, $credentials);
        $dal = DAL::getInstance();
        $person = $dal->personRead(['filters' => $filters]);
        if (!$person) {
            return false;
        }
        // maybe
        if (!is_array($person)) {
            $message = "unexpected result";
            $code = 500 ;
            throw new \Exception($message, $code);
        }
        $person = $person[0];
        //$writeResult = $dal->personTokenCreate($person);
        $token = $dal->tokenCreate($person);
        unset($token->person_id);
        $result = ['person' => $person, 'token' => $token];
        return $result;
    }


    /**
     * Expose DAL::personTokenCreate() to API
     * @param stdClass $person
     * @return object
     */
    public function personTokenCreate(\stdClass $person)
    {
        $dal = DAL::getInstance();
        $token = $dal->tokenCreate($person);
        return $token;
    }


    /**
     * @param object $person
     */
    public function setPerson(\stdClass $person)
    {
        $this->person = $person;
    }
}
